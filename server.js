var express = require('express'),
    log = require('./utils/log.js'),
    app = express();
// Enviromet validation
if (app.get('env') != "production" && app.get('env') != "development") {
    log.error.app('You can only use environments : development or production');
    app.get('env') = 'development';
}
var path = require('path'),
    fs = require('fs'),
    favicon = require('serve-favicon'),
    logger = require('morgan'),
    cookieParser = require('cookie-parser'),
    bodyParser = require('body-parser'),
    errorHandler = require('./utils/errorHandler.js'),
    dbObj = require('./utils/database');

var routes = require('./routes/index'),
    configuration = require('./config/config.json');

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// Documentation
//if (app.get('env') == 'development') {
//    app.use('/docs', function (req, res) {
//        //  serve documentation TODO
//        // return res.redirect('/readme.html');
//    });
//}

// placing your favicon in /public
app.use(favicon(__dirname + '/public/favicon.ico'));
// Time start request
app.use(function (req, res, next) {
    req._responseTime = new Date().getTime();
    next();
});
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: false
}));

app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// PUSH Notification
require('./utils/pushNotification-ios.js').init();
require('./utils/pushNotification-android.js').init();
// EMAIL
require('./utils/mail.js').init();
require('./utils/mail.js').send();

// Bucket Amazon S3
require('./utils/bucket.js').init();

// WELCOME API
/* GET home page. */
app.get('/', function (req, res) {
    res.render('index', {
        title: 'Cactus mode ' + app.get('env'),
        ip: req.headers['x-forwarded-for'] || req.connection.remoteAddress
    });
});
app.get('/hello', function (req, res) {
    res.status(200).json({
        message: " (っ◕‿◕)っ <( nice to meet you)"
    })
});
// APIs
app.use('/api/v1/', routes);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    return next({
        httpCode: 404,
        code: 103,
        message: req.url
    });
});

// error handler
app.use(errorHandler(configuration.appName));

// Setting port and ip
app.set('port', configuration.connection[app.get('env')].port ? configuration.connection[app.get('env')].port : 'localhost');
app.set('ip', configuration.connection[app.get('env')].address ? configuration.connection[app.get('env')].address : 8000);

// Connection
if (configuration.connection[app.get('env')].protocol == "HTTP") {
    log.success.app(configuration.connection[app.get('env')].protocol);
    log.success.app("Cpus : " + require('os').cpus().length);
    log.success.app("Env : " + app.get('env'));
    app.listen(app.get('port'), app.get('ip'), function () {
        log.success.app('Server started on: ' + app.get('ip') + ', at port: ' + app.get('port'));
    });

} else {
    var https = require('https');
    // HTTPS certificate
    try {
        var _hskey = fs.readFileSync(configuration['HTTPS'][app.get('env')].key),
            _hscert = fs.readFileSync(configuration['HTTPS'][app.get('env')].cert);
    } catch (e) {
        return log.error.app('Missing HTTPS certificates');
    }
    var _options = {
        key: _hskey,
        cert: _hscert
    };
    // Start HTTPS server
    https.createServer(_options, app).listen(app.get('port'), app.get('ip'), function () {
        log.success.app(configuration.connection[app.get('env')].protocol);
        log.success.app("Cpus : " + require('os').cpus().length);
        log.success.app("Env : " + app.get('env'));
        log.success.app('Server started on: ' + app.get('ip') + ', at port: ' + app.get('port'));
    });
}

module.exports = app;
