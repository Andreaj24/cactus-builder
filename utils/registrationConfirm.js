var mail = require("nodemailer").mail,
    nodemailer = require("nodemailer"),
    log = require('../utils/log.js');

// Configuration File
var configuration = require('../config/config.json');
var randomToken = require('random-token').create(configuration.APIKEYS.mailToken);

// Create a SMTP transport object
var transport = nodemailer.createTransport("SMTP", {
    //service: 'Gmail', // use well known service.
    // If you are using @gmail.com address, then you don't
    // even have to define the service name
    auth: {
        user: configuration.GMAIL.email,
        pass: configuration.GMAIL.password
    }
});
exports.confirm = function (req, res) {
    if (!req.isBasicAuth) return next();
    var _token = randomToken(16);
    req.db.User.findOne({
        email: req.user.email
    }).exec(function (err, user) {
        var link = configuration.site.mailserver[app.get('env')] + "/api/v1/account/confirm/" + _token;
        console.log("User" + JSON.stringify(user))
        if (err || !user) return res.send(500);
        user.registrationConfirmToken = _token;
        user.save(function (err, user) {
            if (err) return res.send(500);
            var mailText = 'Copia e incolla quwsto link per confermare la tua registraizione: ' + link;
            var message = {
                // sender info
                from: 'Tickete',

                // Comma separated list of recipients
                to: req.user.email,

                // Subject of the message
                subject: "Conferma di registrazione",

                headers: {
                    'X-Laziness-level ': 1000
                },
                // plaintext body
                text: mailText,
                // HTML body
                html: '<a href="' + link + '">Conferma la tua registrazione</a>'

            };
            transport.sendMail(message, function (error) {
                if (error) {
                    log.success.app("[Mail] - Message not sent to  " + req.user.email, {
                        message: message
                    });
                    return next({
                        code: 100,
                        message: "Message not sent to  " + req.user.email,
                        httpCode: 500
                    });
                }
                log.success.app("[Mail] - Message sent successfully to  " + req.user.email, {
                    message: message
                });
                return res.status(200).json({
                    success: true,
                    message: 'Message sent',
                    reponseTime: new Date().getTime() - req._responseTime,
                    data: message
                });
                // if you don't want to use this transport object anymore, uncomment following line
                transport.close(); // close the connection pool
            });

        });
    });
};
