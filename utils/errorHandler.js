//
//                           __
//                          (_ `-~~~~--.
//                            ( _-  (   \
//                        \   _)o)   (  |_
//                           `-,__        )
//                      _         )    )
//                               ---, `
//                         /       ,^^^^_
//                                 |   \  \
//                                 |    \^^\
//                                 ------```
//                                /________\
//                                  _| |__|
//                                 ~--~----~

var log = require('../utils/log.js'),
    _appName = 'JJ';

var _error = [
     // 40X
    {
        code: 1,
        description: _appName + 'InvalidExternalAccessToken'
        },
    {
        code: 2,
        description: _appName + 'RegistrationNeeded'
        },
    {
        code: 3,
        description: _appName + 'LoginFailed'
        },
    {
        code: 4,
        description: _appName + 'InvalidParam'
        },
    {
        code: 5,
        description: _appName + 'PasswordNotMatching'
        },
    {
        code: 6,
        description: _appName + 'InvalidApiKey'
        },
    {
        code: 7,
        description: _appName + 'ImageNeeded'
        },
    {
        code: 8,
        description: _appName + 'UserAlreadyExist'
        },
    {
        code: 9,
        description: _appName + 'ExpiredToken'
        },
    {
        code: 10,
        description: _appName + 'InvalidToken'
        },
    {
        code: 11,
        description: _appName + 'Uroboro' // user con not follow itself
        },
    {
        code: 12,
        description: _appName + 'PostNotExist'
        },
    {
        code: 13,
        description: _appName + 'PostStatusNotValid'
        },
    {
        code: 14,
        description: _appName + 'RoleTooLow'
        },
    {
        code: 15,
        description: _appName + 'CouponNotExist'
        },
    {
        code: 16,
        description: _appName + 'ScratchedNotExist'
        },
    {
        code: 17,
        description: _appName + 'CouponShouldBeScratched'
        },
              // 50X
    {
        code: 100,
        description: _appName + 'GenericError'
        },
    {
        code: 101,
        description: _appName + 'AmazonExternalError'
        },
    {
        code: 102,
        description: _appName + 'DatabaseError'
        },
    {
        code: 103,
        description: _appName + 'ApiNotFound'
        }
    ];

module.exports = function (appName) {
    _appName = appName.substring(0, 2).toUpperCase();
    return function (err, req, res, next) {
        try {
            var ErrorResponseFormatter = {};
            var code = err ? err.code : 100;
            var httpCode = err ? Number(err.httpCode) : 500;

            for (var i in _error) {
                if (_error[i].code == code) {
                    ErrorResponseFormatter = _error[i];
                } else {
                    if (i >= _error.length)
                        res.json(500, {
                            error: 'Error code in error Handler, code ' + code + 'dont exist'
                        });
                }
            };
            ErrorResponseFormatter.details = err.errors ? err.errors : undefined
            return res.status(httpCode).json({
                success: false,
                message: err ? err.message : 'error',
                reponseTime: new Date().getTime() - req._responseTime,
                error: ErrorResponseFormatter
            });
        } catch (e) {
            res.status(500).json({
                success: false,
                "message": "critical",
                reponseTime: new Date().getTime() - req._responseTime,
                "error": e
            });
        }
    }
};
