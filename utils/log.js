/*
Author: Andrea Giglio @2014
v.0.2.0
Color Log library
Structure
            [TypeOfError] - errorMessages

[TypeOfError]  Color
                    Red    : Error
                    Green  : Correct
                    Yellow : Database
                    Blue   : api
                    Cyan   : mobile
errorMessages Color
                    Red    : Error
                    Green  : Correct
                    grey   : Info
                    Yellow : Warn
*/
var colors = require('colors'),
    util = require('util'),
    winston = require('winston'),
    fs = require('fs');

// App instance
var app = require('express')();
var _params = function (params) {
    return 'testing' == app.get('env') ? params : undefined
}

// test if exist logs folder, if not create
if (!fs.existsSync('./logs')) {
    console.log("[APP] - ".magenta + "Created Folder ./logs".green);
    fs.mkdirSync('./logs');
}
// Transoport setting winston
var logger = new(winston.Logger)({
    transports: [
     // new(winston.transports.Console)(),
      new(winston.transports.File)({
            filename: './logs/TKLog.log'
        })
    ]
});
var loggerErrors = new(winston.Logger)({
    transports: [
     // new(winston.transports.Console)(),
      new(winston.transports.File)({
            filename: './logs/TKErrors.log'
        })
    ]
});
var loggerAccess = new(winston.Logger)({
    transports: [
     // new(winston.transports.Console)(),
      new(winston.transports.File)({
            filename: './logs/TKAccess.log'
        })
    ]
});
// compose extra params in log
var _withParams = function (obj, color1, color2) {
    if (obj) return ("\n[--")[color1] + "params" + ("--] ")[color1] + JSON.stringify(obj, null, 2)['yellow'];
    return "";
};
// compose log and relative colors
var _makeMyog = function (type, msg, params, color1, color2, color3) {
    var _d = new Date();
    var _log = ('[' + type + '] - ')[color1] + msg[color2] + (' (' + _d.toDateString() + _d.toTimeString() + ')')[color3] + _withParams(params, color1, color3);
    return _log;
};
var _logFileMaker = function (type, msg, params, isError) {
    if (!isError) {
        logger.log("info", "[" + type + "] - %s ", msg);
        if (params)
            logger.log("info", "[" + type + "] - [PARAMS] %j ", params);
    } else {
        loggerErrors.log("error", "[" + type + "] - %s ", msg);
        if (params)
            loggerErrors.log("error", "[" + type + "] - [PARAMS] %s ", params);
    }
};

// Module for logs
module.exports = {
    // Default normal log
    log: function (msg, params) {
        var type = 'DEBUG';
        console.log(_makeMyog(type, msg, params, 'blue', 'blue', 'blue'));
        logger.log('info', "App login");
        _logFileMaker(type, msg, params)
    },
    logged: function (user) {
        /*
            user:{
            name:"nome",
            id:"0382948468",
            token:"sdfsdfdfy987g68g"
            }
        */
        var type = "USER-LOGGED"
        loggerAccess.log("info", "[" + type + "] %s is now logged, with this id:  %s , and this token: %s", user.name, user.id, user.token);
    },
    success: {
        color: 'green',
        database: function (msg, params) {
            params = _params(params);
            var type = 'DATABASE';
            console.log(_makeMyog(type, msg, params, 'yellow', this.color, 'grey'));
            _logFileMaker(type, msg, params);
        },

        api: function (msg, params) {
            params = _params(params);
            var type = 'API';
            console.log(_makeMyog(type, msg, params, 'blue', this.color, 'grey'));
            _logFileMaker(type, msg, params);
        },
        app: function (msg, params) {
            params = _params(params);
            var type = "APP"
            console.log(_makeMyog(type, msg, params, 'magenta', this.color, 'grey'));
            _logFileMaker(type, msg, params);
        },
        mobile: function (msg, params) {
            params = _params(params);
            var type = 'MOBILE';
            console.log(_makeMyog(type, msg, params, 'cyan', this.color, 'grey'));
            _logFileMaker(type, msg, params);
        },
        default: function (msg, params) {
            params = _params(params);
            var type = 'DEFAULT';
            console.log(_makeMyog(type, msg, params, 'gray', this.color, 'grey'));
            _logFileMaker(type, msg, params);
        }
    },
    error: {
        color: 'red',
        database: function (msg, params) {
            params = _params(params);
            var type = 'DATABASE';
            console.log(_makeMyog(type, msg, params, 'yellow', this.color, 'grey'));
            _logFileMaker(type, msg, params, true);
        },
        api: function (msg, params) {
            params = _params(params);
            var type = 'API';
            console.log(_makeMyog(type, msg, params, 'blue', this.color, 'grey'));
            _logFileMaker(type, msg, params, true);
        },
        mobile: function (msg, params) {
            params = _params(params);
            var type = 'MOBILE';
            console.log(_makeMyog(type, msg, params, 'cyan', this.color, 'grey'));
            _logFileMaker(type, msg, params, true);
        },
        app: function (msg, params) {
            params = _params(params);
            var type = 'APP';
            console.log(_makeMyog(type, msg, params, 'magenta', this.color, 'grey'));
            _logFileMaker(type, msg, params, true);
        },
        default: function (msg, params) {
            params = _params(params);
            var type = 'DEFAULT';
            console.log(_makeMyog(type, msg, params, 'gray', this.color, 'grey'));
            _logFileMaker(type, msg, params, true);
        }
    },
    info: {
        color: 'grey',
        database: function (msg, params) {
            params = _params(params);
            var type = 'DATABASE';
            console.log(_makeMyog(type, msg, params, 'yellow', this.color, 'gray'));
            // _logFileMaker(type, msg, params);
        },
        api: function (msg, params) {
            params = _params(params);
            var type = 'API';
            console.log(_makeMyog(type, msg, params, 'blue', this.color, 'grey'));
            //  _logFileMaker(type, msg, params);
        },
        mobile: function (msg, params) {
            params = _params(params);
            var type = 'MOBILE';
            console.log(_makeMyog(type, msg, params, 'cyan', this.color, 'gray'));
            // _logFileMaker(type, msg, params);
        },
        default: function (msg, params) {
            params = _params(params);
            var type = 'DEFAULT';
            console.log(_makeMyog(type, msg, params, 'yellow', this.color, 'grey'));
            //   _logFileMaker(type, msg, params);
        }
    },
    warn: {
        color: 'magenta',
        database: function (msg, params) {
            params = _params(params);
            var type = 'DATABASE';
            console.log(_makeMyog(type, msg, params, 'yellow', this.color, 'grey'));
            _logFileMaker(type, msg, params);
        },
        api: function (msg, params) {
            params = _params(params);
            var type = 'API';
            console.log(_makeMyog(type, msg, params, 'blue', this.color, 'grey'));
            _logFileMaker(type, msg, params);
        },
        mobile: function (msg, params) {
            params = _params(params);
            var type = 'MOBILE';
            console.log(_makeMyog(type, msg, params, 'cyan', this.color, 'grey'));
            _logFileMaker(type, msg, params);
        },
        default: function (msg, params) {
            params = _params(params);
            var type = 'DEFAULT';
            console.log(_makeMyog(type, msg, params, 'yellow', this.color, 'grey'));
            _logFileMaker(type, msg, params);
        }

    }
};
